#/usr/bin/env bash

set -e

echo "⚠️  Please enter your password if requested."

SCRIPT_HOME=$(cd "$(dirname $0)" && pwd)

hosts="$SCRIPT_HOME/../hosts"

while IFS= read -r line
do 
    # insert/update hosts entry
    ip_address=$(echo $line | cut -d' ' -f1)
    host_name=$(echo $line | cut -d' ' -f2)

    # find existing instances in the host file and save the line numbers
    matches_in_hosts="$(grep -n $host_name /etc/hosts | cut -f1 -d:)"
    host_entry="${ip_address} ${host_name} # generated"

    if [ ! -z "$matches_in_hosts" ]
    then
        echo "Updating existing hosts entry: $host_name"
        # iterate over the line numbers on which matches were found
        while read -r line_number; do
            # replace the text of each line with the desired host entry
            sudo sed -i '' "${line_number}s/.*/${host_entry} /" /etc/hosts
        done <<< "$matches_in_hosts"
    else
        echo "Adding new hosts entry: $host_name"
        echo "$host_entry" | sudo tee -a /etc/hosts > /dev/null
    fi
done < "$hosts"

exit 0

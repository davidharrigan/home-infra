update-hosts:
	./scripts/update-hosts.sh

galaxy:
	ansible-galaxy install -r provision/requirements.yml

encrypt:
	ansible-vault encrypt provision/vars/secrets.yml --vault-password-file vault_pass

decrypt:
	ansible-vault decrypt provision/vars/secrets.yml --vault-password-file vault_pass

provision/local:
	ansible-playbook \
		--private-key=.vagrant/machines/default/virtualbox/private_key \
		-u vagrant \
		-i ./provision/inventory/local \
		--vault-password-file vault_pass \
		provision/playbook.yml

provision/local/compose:
	ansible-playbook \
		--private-key=.vagrant/machines/default/virtualbox/private_key \
		-u vagrant \
		-i ./provision/inventory/local \
		--tags "compose" \
		--vault-password-file vault_pass \
		provision/playbook.yml

provision/coffee:
	ansible-playbook -vv \
		-k \
		-i ./provision/inventory/coffee \
		--vault-password-file vault_pass \
		-K \
		provision/playbook.yml

provision/coffee/compose:
	ansible-playbook \
		-k \
		-i ./provision/inventory/ \
		--tags "compose" \
		--vault-password-file vault_pass \
		-K \
		provision/playbook.yml

up: up/core up/media

up/core:
	docker-compose -f compose/core.yml up -d

up/media:
	docker-compose -f compose/media.yml up -d

